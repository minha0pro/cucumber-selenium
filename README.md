# Cucumber and Selenium Example (NodeJS)

## Pre requirement

NodeJS

## Run test and report

Test

`npm run test`

Report

`npm run report`

## Reference
https://cucumber.io/
https://cucumber.io/docs/guides/10-minute-tutorial/
