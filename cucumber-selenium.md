## Cucumber
Là một open source tool thực hiện test nghiệp vụ cho hệ thống. Nghiệp vụ được viết theo cách mọi người đều đọc được (business-readable specifications).

Do đó, người phân tích nghiệp vụ có thể viết nghiệp vụ ra file và file này sẽ được dùng để chạy test tự động. Các file nghiệp vụ này được quản lý cùng với source code. Điều này đảm bảo nghiệp vụ và code luôn luôn song hành.

Hướng phát triển phần mềm như vậy còn được gọi là Behaviour-Driven Development (BDD). Cucumber được xem là công cụ số một trong BDD.

Tham khảo:
https://en.wikipedia.org/wiki/Behavior-driven_development
https://cucumber.io/docs/bdd/

### Gherkin
Nghiệp vụ hay chức năng hệ thống được viết dưới dạng text thông thường để mọi người đều đọc và hiểu được. Dạng text này sẽ theo một format hay các luật ngữ pháp nhất định gọi là Gherkin.

Mô tả chức năng sẽ được lưu trong các file text có đuôi `.feature` và thường được quản lý cùng với source code của phần mềm. Cucumber sẽ đọc các file này và thực thi các bước (steps) tương ứng để test phần mềm. Vì vậy có thể gọi các file này là `executable specifications`.

Ví dụ một file `.feature` 

```Gherkin 
Feature: Returns and exchanges go to inventory.

    As a store owner, 
    I want to add items back to inventory when they are returned or exchanged, 
    so that I can track inventory.

    Scenario 1: Items returned for refund should be added to inventory.
        Given that a customer previously bought a black sweater from me
        And I have three black sweaters in inventory,
        When they return the black sweater for a refund,
        Then I should have four black sweaters in inventory.

    Scenario 2: Exchanged items should be returned to inventory.
        Given that a customer previously bought a blue garment from me
        And I have two blue garments in inventory
        And three black garments in inventory,
        When they exchange the blue garment for a black garment,
        Then I should have three blue garments in inventory
        And two black garments in inventory.
```

Gherkin sử dụng một tập các keywords để mô tả cấu trúc và ngữ nghĩa của executable specifications.

**Feature**
Keyword mô tả chức năng của hệ thống, nhóm các kịch bản test (scenarios) liên quan lại với nhau.

Ngay dưới keyword `Feature`, bạn có thể thêm text mô tả chức năng.

**Scenario**
Keyword mô tả yêu cầu nghiệp hay kịch bản test. Mỗi `Scenario` sẽ bao gồm các steps. Các steps được bước đầu bằng các keywords: 
* `Given` Mô tả ngữ cảnh ban đầu cần được khởi tạo - initial context
* `When` Mô tả sự kiện - event
* `Then` Mô tả đầu ra mong muốn - expected outcome 

Nếu bạn có nhiều `Given`'s, `When`’s, or `Then`'s thì bạn có thể thay thế bằng `And`, `But` cho dễ đọc.

```Gherkin
Scenario: Multiple Givens
  Given one thing
  Given another thing
  Given yet another thing
  When I open my eyes
  Then I should see something
  Then I shouldn't see something else
```

Chuyển thành 

```Gherkin
Scenario: Multiple Givens
  Given one thing
  And another thing
  And yet another thing
  When I open my eyes
  Then I should see something
  But I shouldn't see something else
``` 

### Step Definitions
Cucumber sẽ đọc file `.feature` và chạy các steps trong từng `Scenario`, nhưng việc từng steps này được chạy ra sao thì cần thông quan `Step Definitions`.

Step definitions sẽ kết nối Gherkin steps với programming code. Máy tính cần thứ gì đó nó hiểu, và do đó bạn cần định nghĩa steps chạy như thế nào bằng ngôn ngữ lập trình.

`Steps in Gherkin` --matched with--> `Step Definitions` --manipulates--> `System`

Ví dụ step definition trong JavaScript 

```JS 
When("{maker} starts a game", function(maker) {
  maker.startGameWithWord({ word: "whale" })
})
```

Tham khảo: https://cucumber.io/docs/gherkin/reference/

### Cucumber Tutorial 

Tham khảo: https://cucumber.io/docs/guides/10-minute-tutorial/

Bạn cần cài Node.js trước tiên sau đó thực hiện các câu lệnh sau để tạo project 

```sh
mkdir hellocucumber
cd hellocucumber
npm init --yes
npm install cucumber --save-dev
mkdir features
mkdir features/step_definitions
```

Mở file `package.json` vào sửa phần test giống như sau:

```JSON
{
  "name": "hellocucumber",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "cucumber-js"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "cucumber": "^6.0.4"
  }
}
```

Tạo một file `cucumber.js` tại gốc của project (root path) và thêm nội dung sau vào:

```JS
module.exports = {
  default: `--format-options '{"snippetInterface": "synchronous"}'`
}
```

Cũng, tạo một file `features/step_definitions/stepdefs.js` với nội dụng sau:

```JS
const assert = require('assert');
const { Given, When, Then } = require('cucumber');
```

Kiểm chứng thành công bằng cách chạy

```sh
# Run via NPM
npm test
```

Tạo feature và steps theo hướng dẫn https://cucumber.io/docs/guides/10-minute-tutorial/

### Cucumber Report 

Cài lib report 

`npm install cucumber-html-reporter --save-dev`

Tạo thư mục `report` và tạo file `cucumber-html-report.js` với nội dung:

```JS
var reporter = require('cucumber-html-reporter');
 
var options = {
        theme: 'bootstrap',
        jsonFile: 'report/cucumber_report.json',
        output: 'report/cucumber_report.html',
        reportSuiteAsScenarios: true,
        scenarioTimestamp: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "STAGING",
            "Browser": "Chrome  54.0.2840.98",
            "Platform": "Windows 10",
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    };
 
reporter.generate(options);
```
Sửa phần test trong file `package.json` và thêm command report

```JSON
"scripts": {
    "test": "cucumber-js -f summary -f json:report/cucumber_report.json",
    "report": "node ./report/cucumber-html-report.js"
  },
```

Chạy test  

`npm run test`

Chạy report 

`npm run report`

## Selenium
Selenium là công cụ tự động hoá browsers. Được sử dụng để test tự động cho các ứng dụng Web. Selenium hỗ trợ rất nhiều các trình duyệt khác nhau (chrome, firefox, IE, ...) và hỗ trợ nhiều ngôn ngữ lập trình để viết test (Java, JavaScript, Python, Ruby, C#, PHP). 

Code test sử dụng API và Selenium-WebDriver sẽ gọi trực tiếp tới browser để thực hiện thao tác test.  

### Selenium-WebDriver Tutorial

Tham khảo

https://www.npmjs.com/package/selenium-webdriver

Cài lib

`npm install selenium-webdriver --save-dev`

Cài driver

`npm install chromedriver`

Tạo file feature `features/google_search.feature`

```Gherkin
Feature: Google search

   Test google search page

   Scenario: Finding some cheese
      Given I am on the Google search page
      When I search for "Cheese!"
      Then the page title should start with "cheese"
``` 

Tạo file định nghĩa steps `step_definitions/cheese.js`

```JS
const { Given, When, Then, AfterAll } = require('cucumber');
const {Builder, By, Key} = require('selenium-webdriver');
const assert = require('assert')
  
const driver = new Builder()
    .forBrowser('chrome')
    .build();

Given('I am on the Google search page', async function () {
    await driver.get('http://www.google.com');
});

When('I search for {string}', async function (searchTerm) {
    const element = await driver.findElement(By.name('q'));
    element.sendKeys(searchTerm, Key.RETURN);
    element.submit();
});

Then('the page title should start with {string}', {timeout: 60 * 1000}, async function (searchTerm) {
    const title = await driver.getTitle();
    const isTitleStartWithCheese = title.toLowerCase().lastIndexOf(`${searchTerm}`, 0) === 0;
    assert.equal(isTitleStartWithCheese, true);
});

AfterAll('end', async function(){
    await driver.quit();
});
```

Chạy test và report

`npm run test`
`npm run report`

### Selenium-WebDriver Commands

**Fetching a Page**

driver.get('http://www.google.com');

**Locating UI Elements (WebElements)**

By ID 

<div id="coolestWidgetEvah">...</div>

var element = driver.findElement(By.id('coolestWidgetEvah'));

By Class Name

<div class="cheese"><span>Cheddar</span></div><div class="cheese"><span>Gouda</span></div>

driver.findElements(By.className("cheese"))
.then(cheeses => console.log(cheeses.length));

By Tag Name

<iframe src="..."></iframe>

var frame = driver.findElement(By.tagName('iframe'));

By Name 

<input name="cheese" type="text"/>

var cheese = driver.findElement(By.name('cheese'));

By XPath

<input type="text" name="example" />
<input type="text" name="other" />

driver.findElements(By.xpath("//input"))
.then(cheeses => console.log(cheeses.length));

**Getting text values**

var element = driver.findElement(By.id('elementID'));
element.getText().then(text => console.log(`Text is ${text}`));

**User Input - Filling In Forms**

driver.findElement(By.id('elementID')).sendKeys("your value");

**Click**

driver.findElement(By.id('submit').click();

**Navigate**

driver.navigate().forward();
driver.navigate().back();
