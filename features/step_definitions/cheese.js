const { Given, When, Then, AfterAll } = require('cucumber');
const {Builder, By, Key} = require('selenium-webdriver');
const assert = require('assert')
  
const driver = new Builder()
    .forBrowser('chrome')
    .build();

Given('I am on the Google search page', async function () {
    await driver.get('http://www.google.com');
});

When('I search for {string}', async function (searchTerm) {
    const element = await driver.findElement(By.name('q'));
    element.sendKeys(searchTerm, Key.RETURN);
    element.submit();
});

Then('the page title should start with {string}', {timeout: 60 * 1000}, async function (searchTerm) {
    const title = await driver.getTitle();
    const isTitleStartWithCheese = title.toLowerCase().lastIndexOf(`${searchTerm}`, 0) === 0;
    assert.equal(isTitleStartWithCheese, true);
});

AfterAll('end', async function(){
    await driver.quit();
});
