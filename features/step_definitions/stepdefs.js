const assert = require('assert')
const { Given, When, Then } = require('cucumber')
const { isItFriday } = require('../../src/time')

Given('today is {string}', function (day) {
    this.today = day
});

When('I ask whether it\'s Friday yet', function () {
    this.actualAnswer = isItFriday(this.today)
});

Then('I should be told {string}', function (expectedAnswer) {
    assert.equal(this.actualAnswer, expectedAnswer)
});
